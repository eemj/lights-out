﻿using LightsOut.Manangers;
using LightsOut.Models;
using LightsOut.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightsOut.Testing.Mocking
{
    public class RoundServiceMock : IRoundService
    {
        private readonly Round _round;

        private readonly IRoundManager _manager;

        // A mock service for test purposes. An easy 5x5 grid has been generated which
        // contains a + sign in the middle.
        public RoundServiceMock(Guid guid)
        {
            _round = new Round
            {
                ID = guid,
                Moves = new List<Move>(),
                Size = 5,
                Rows = Enumerable.Range(0, 5)
                    .Select((_, rowIndex) => new Row
                    {
                        Position = rowIndex,
                        Columns = Enumerable.Range(0, 5)
                            .Select((_, columnIndex) => new Column { Position = columnIndex })
                            .ToList(),
                    })
                    .ToList()
            };

            // Create a cross in the middle.
            _round.Rows.ElementAt(1).Columns.ElementAt(2).On = true;
            _round.Rows.ElementAt(2).Columns.ElementAt(1).On = true;
            // Making a move at these co-ords should technically win the game.
            _round.Rows.ElementAt(2).Columns.ElementAt(2).On = true;
            _round.Rows.ElementAt(2).Columns.ElementAt(3).On = true;
            _round.Rows.ElementAt(3).Columns.ElementAt(2).On = true;

            _manager = new RoundManager();
        }

        public Round Create() => _round;

        public void Forfeit(Guid gid)
        {
            _round.State = RoundState.Canceled;
        }

        public Round GetByID(Guid gid)
        {
            if (gid.Equals(_round.ID))
                return _round;

            return null;
        }

        public Round Move(Round round, Move move) => _manager.Move(round, move);
    }
}
