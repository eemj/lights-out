﻿using LightsOut.Contracts;
using LightsOut.Controllers;
using LightsOut.Models;
using LightsOut.Services;
using LightsOut.Testing.Mocking;
using Microsoft.AspNetCore.Mvc;
using System;
using Xunit;

namespace LightsOut.Testing
{
    public class RoundControllerTest
    {
        private readonly RoundController _controller;
        private readonly IRoundService _service;

        private readonly Guid GUID = new Guid("c58967e1-f0d4-4442-92e8-2c03cc0bb762");

        public RoundControllerTest()
        {
            _service = new RoundServiceMock(GUID);
            _controller = new RoundController(_service);
        }

        /// <summary>
        /// Passing a random GUID instead of the created one, should throw
        /// a 404 status along with a not found message.
        /// </summary>
        [Fact]
        public void Move_RoundNotFound()
        {
            var response = _controller.Move(Guid.NewGuid(), new Move
            {
                X = 1,
                Y = 1
            });

            Assert.IsType<NotFoundObjectResult>(response);
            Assert.IsType<ErrorResponse>((response as NotFoundObjectResult).Value);
            ErrorResponse error = ((response as NotFoundObjectResult).Value as ErrorResponse);
            Assert.Contains("not found", error.Error);
        }

        /// <summary>
        /// Passing X or Y coords that are greater than the grid size itself, should
        /// throw a 400 status along with an out of bounds message.
        /// </summary>
        [Fact]
        public void Move_OutOfBounds()
        {
            var response = _controller.Move(GUID, new Move
            {
                X = 5,
                Y = 5,
            });

            Assert.IsType<BadRequestObjectResult>(response);
            Assert.IsType<ErrorResponse>((response as BadRequestObjectResult).Value);
            ErrorResponse error = (response as BadRequestObjectResult).Value as ErrorResponse;
            Assert.Contains("out of bounds", error.Error);
        }

        /// <summary>
        /// Making a move to win the game, it should throw a 200 status and the round
        /// state should be updated to RoundState.Won.
        /// </summary>
        [Fact]
        public void Move_RoundWin()
        {
            var response = _controller.Move(GUID, new Move
            {
                X = 2,
                Y = 2,
            });

            Assert.IsType<OkObjectResult>(response);
            Assert.IsType<Round>((response as OkObjectResult).Value);
            Round round = (response as OkObjectResult).Value as Round;
            Assert.True(round.State.Equals(RoundState.Won));
        }
    }
}
