﻿using LightsOut.Models;
using LightsOut.Services;
using LightsOut.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;

namespace LightsOut.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SettingController : ControllerBase
    {
        private readonly ISettingService _service;

        public static readonly string MAX_LIGHTS_ON = "MAX_LIGHTS_ON";
        public static readonly string MIN_LIGHTS_ON = "MIN_LIGHTS_ON";
        public static readonly string GRID_SIZE = "GRID_SIZE";
        public static readonly string FUN_MODE = "FUN_MODE";

        public SettingController(ISettingService service)
        {
            _service = service;
        }

        [HttpGet]
        public IEnumerable<Setting> GetAll() => _service.GetAll();

        [HttpPatch]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(Setting))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorResponse))]
        public IActionResult Update([FromBody] Setting request)
        {
            if (string.IsNullOrEmpty(request.Name))
                return BadRequest(new ErrorResponse("Name field is missing."));

            if (!request.Name.Equals(FUN_MODE) && request.Value <= 0)
                return BadRequest(new ErrorResponse("Value must be greater than 0."));

            // Ensure that the grid size is not less than 5
            if (request.Name.Equals(GRID_SIZE) && request.Value < 5)
                return BadRequest(new ErrorResponse("Grid size cannot be less than 5."));

            // Ensure that the maximum is greater than the minimum.
            if (request.Name.Equals(MAX_LIGHTS_ON))
            {
                IEnumerable<Setting> settings = _service.GetAll();

                if (settings.Any(setting => setting.Name.Equals(MIN_LIGHTS_ON) && setting.Value > setting.Value))
                    return BadRequest(new ErrorResponse("'MIN_LIGHTS_ON' value should be less than the 'MAX_LIGHTS_ON'."));
            }

            // Ensure that the minimum is greater than the maximum.
            if (request.Name.Equals(MIN_LIGHTS_ON))
            {
                IEnumerable<Setting> settings = _service.GetAll();

                if (settings.Any(setting => setting.Name.Equals(MAX_LIGHTS_ON) && setting.Value < setting.Value))
                    return BadRequest(new ErrorResponse("'MAX_LIGHTS_ON' value should be greater than the 'MIN_LIGHTS_ON'."));
            }

            // Fun mode must be 1 or 0
            if (request.Name.Equals(FUN_MODE) && (request.Value < 0 || request.Value > 1))
            {
                return BadRequest(new ErrorResponse("'FUN_MODE' must be either 0 or 1."));
            }

            Setting setting = _service.GetByName(request.Name);

            if (setting == null)
            {
                return NotFound(new ErrorResponse(string.Format(
                    "Setting named '{0}' not found.",
                    request.Name
                )));
            }

            setting = _service.Update(setting.Name, request.Value);

            return Ok(setting);
        }
    }
}
