﻿using LightsOut.Contracts;
using LightsOut.Models;
using LightsOut.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net.Mime;

namespace LightsOut.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoundController : ControllerBase
    {
        private readonly IRoundService _service;

        public RoundController(IRoundService service)
        {
            _service = service;
        }

        // GET api/<GameController>/<gid>
        [HttpGet("{gid}")]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErrorResponse))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Round))]
        public IActionResult Get(Guid gid)
        {
            Round round = _service.GetByID(gid);

            if (round == null)
                return NotFound(new ErrorResponse("Round not found"));

            return Ok(round);
        }

        // POST api/<GameController>/<gid>
        [HttpPost("{gid}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErrorResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorResponse))]
        [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(Round))]
        public IActionResult Move(Guid gid, [FromBody] Move move)
        {
            Round round = _service.GetByID(gid);

            // Return 404 if we didn't find the round.
            if (round == null)
                return NotFound(new ErrorResponse("Round not found"));

            // Return 400 if the round state is called or won.
            if (round.State == RoundState.Canceled || round.State == RoundState.Won)
                return BadRequest(new ErrorResponse("Moves are only applied when the round is in progress"));

            // Return 400 if the move is out of bounds (gridSize = 5 and user specified X to be 6).
            if (move.X < 0 || move.Y < 0 || (round.Size - 1) < move.Y || (round.Size - 1) < move.X)
                return BadRequest(new ErrorResponse("Move is out of bounds"));

            round = _service.Move(round, move);

            return Ok(round);
        }

        // GET api/<GameController>/Create
        [HttpGet]
        [Route("Create")]
        public Round Create() => _service.Create();

        // DELETE api/<GameController>/<gid>
        [HttpDelete("{gid}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorResponse))]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        public IActionResult Forfeit(Guid gid)
        {
            Round round = _service.GetByID(gid);

            if (round == null)
                return NotFound(new ErrorResponse("Round not found"));

            if (round.State == RoundState.Canceled)
                return BadRequest(new ErrorResponse("Round is already canceled"));

            _service.Forfeit(gid);

            return Accepted();
        }
    }
}
