﻿using LightsOut.Models;

namespace LightsOut.Manangers
{
    public interface IRoundManager
    {
        /// <summary>
        /// Move handles light toggling on and around the provided X and Y positions.
        /// </summary>
        /// <param name="round">The round to apply moves on.</param>
        /// <param name="move">A move which contains both X and Y positions.</param>
        /// <returns></returns>
        public Round Move(Round round, Move move);

        /// <summary>
        /// Forfeit changes the provided round state to Canceled.
        /// </summary>
        /// <param name="round">The round to change state for.</param>
        /// <returns></returns>
        public Round Forfeit(Round round);
    }
}
