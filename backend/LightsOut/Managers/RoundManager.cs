﻿using LightsOut.Models;
using System.Linq;

namespace LightsOut.Manangers
{
    public class RoundManager : IRoundManager
    {
        public Round Move(Round round, Move move)
        {

            round.Rows.ElementAt(move.Y).Columns.ElementAt(move.X).On =
                !round.Rows.ElementAt(move.Y).Columns.ElementAt(move.X).On;

            // Toggle the upper element if it's not out of bounds.
            if (round.Rows.Count - 1 > move.Y)
            {
                round.Rows.ElementAt(move.Y + 1).Columns.ElementAt(move.X).On =
                    !round.Rows.ElementAt(move.Y + 1).Columns.ElementAt(move.X).On;
            }

            // Toggle the lower element if it's not out of bounds.
            if (move.Y > 0)
            {
                round.Rows.ElementAt(move.Y - 1).Columns.ElementAt(move.X).On =
                    !round.Rows.ElementAt(move.Y - 1).Columns.ElementAt(move.X).On;
            }

            // Toggle the previous element if it's not out of bounds.
            if (round.Rows.ElementAt(move.Y).Columns.Count - 1 > move.X)
            {
                round.Rows.ElementAt(move.Y).Columns.ElementAt(move.X + 1).On =
                    !round.Rows.ElementAt(move.Y).Columns.ElementAt(move.X + 1).On;
            }

            // Toggle the next element if it's not out of bounds.
            if (move.X > 0)
            {
                round.Rows.ElementAt(move.Y).Columns.ElementAt(move.X - 1).On =
                    !round.Rows.ElementAt(move.Y).Columns.ElementAt(move.X - 1).On;
            }

            // Add the move.
            round.Moves.Add(move);

            // If the round state is still Generated, we'll update the state to in progress.
            if (round.State != RoundState.InProgress)
                round.State = RoundState.InProgress;

            // We're going to check this round is won. For a user to win, all the lights must be off.
            if (!round.Rows.Any(row => row.Columns.Any(column => column.On)))
                round.State = RoundState.Won;

            return round;
        }

        public Round Forfeit(Round round)
        {
            round.State = RoundState.Canceled;
            return round;
        }
    }
}
