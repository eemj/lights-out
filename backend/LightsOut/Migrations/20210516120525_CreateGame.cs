﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LightsOut.Migrations
{
    public partial class CreateGame : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Rounds",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Size = table.Column<int>(type: "int", nullable: false),
                    State = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rounds", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Settings",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Value = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Settings", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "GridRows",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Position = table.Column<int>(type: "int", nullable: false),
                    RoundID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GridRows", x => x.ID);
                    table.ForeignKey(
                        name: "FK_GridRows_Rounds_RoundID",
                        column: x => x.RoundID,
                        principalTable: "Rounds",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Moves",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    X = table.Column<int>(type: "int", nullable: false),
                    Y = table.Column<int>(type: "int", nullable: false),
                    RoundID = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Moves", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Moves_Rounds_RoundID",
                        column: x => x.RoundID,
                        principalTable: "Rounds",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GridColumns",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    On = table.Column<bool>(type: "bit", nullable: false),
                    Position = table.Column<int>(type: "int", nullable: false),
                    RowID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GridColumns", x => x.ID);
                    table.ForeignKey(
                        name: "FK_GridColumns_GridRows_RowID",
                        column: x => x.RowID,
                        principalTable: "GridRows",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Settings",
                columns: new[] { "ID", "Name", "Value" },
                values: new object[,]
                {
                    { new Guid("1feac285-5c53-411b-8dec-51d7af1c0ced"), "GRID_SIZE", 5 },
                    { new Guid("1ac1242f-5a4d-4006-abf5-5bcfa856aa73"), "MAX_LIGHTS_ON", 5 },
                    { new Guid("5eaf3990-5635-4aa8-92a4-dc7401c11ac8"), "MIN_LIGHTS_ON", 1 },
                    { new Guid("118ea1af-ce92-4af7-9055-217873074f69"), "FUN_MODE", 0 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_GridColumns_RowID",
                table: "GridColumns",
                column: "RowID");

            migrationBuilder.CreateIndex(
                name: "IX_GridRows_RoundID",
                table: "GridRows",
                column: "RoundID");

            migrationBuilder.CreateIndex(
                name: "IX_Moves_RoundID",
                table: "Moves",
                column: "RoundID");

            migrationBuilder.CreateIndex(
                name: "IX_Settings_Name",
                table: "Settings",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GridColumns");

            migrationBuilder.DropTable(
                name: "Moves");

            migrationBuilder.DropTable(
                name: "Settings");

            migrationBuilder.DropTable(
                name: "GridRows");

            migrationBuilder.DropTable(
                name: "Rounds");
        }
    }
}
