﻿using LightsOut.DBContexts;
using LightsOut.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LightsOut.Services
{
    public class SettingService : ISettingService
    {
        private readonly LightsOutContext _context;

        public SettingService(LightsOutContext context)
        {
            _context = context;
        }

        public Setting GetByName(string name) => _context.Settings
            .Where(setting => setting.Name.Equals(name))
            .FirstOrDefault();

        public IEnumerable<Setting> GetAll() => _context.Settings.ToList();

        public Setting Update(string name, int value)
        {
            Setting setting = _context.Settings
                .Where(setting => setting.Name.Equals(name))
                .FirstOrDefault();

            setting.Value = value;

            _context.Settings.Update(setting);
            _context.SaveChanges();

            return setting;
        }
    }
}
