﻿using LightsOut.DBContexts;
using LightsOut.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Net;
using System.Linq;
using System.Web.Http;
using System.Net.Http;
using LightsOut.Manangers;

namespace LightsOut.Services
{
    public class RoundService : IRoundService
    {
        private readonly LightsOutContext _context;

        private readonly Random _rnd;

        private readonly IRoundManager _manager;

        private const string SettingGridSize = "GRID_SIZE";
        private const string SettingMaxLightsOn = "MAX_LIGHTS_ON";
        private const string SettingMinLightsOn = "MIN_LIGHTS_ON";

        public RoundService(LightsOutContext context)
        {
            _context = context;
            _rnd = new Random();
            _manager = new RoundManager();
        }

        // GetSetting returns the setting value of the passed setting name.
        private int GetSetting(string name) => _context.Settings.Where(setting => setting.Name.Equals(name)).FirstOrDefault().Value;

        // GetGridSize returns the value of the 'GRID_SIZE' setting.
        private int GetGridSize() => GetSetting(SettingGridSize);

        // GetMaxLightsOn returns the value of the 'MAX_LIGHTS_ON' setting.
        private int GetMaxLightsOn() => GetSetting(SettingMaxLightsOn);

        // GetMinLightsOn returns the value of the 'MIN_LIGHTS_ON' setting.
        private int GetMinLightsOn() => GetSetting(SettingMinLightsOn);

        // GetByID returns the round that's associated to the passed ID.
        public Round GetByID(Guid gid) => _context.Rounds
            .Where(round => round.ID.Equals(gid))
            .Include(round => round.Moves)
            .Include(round => round.Rows
                .OrderBy(row => row.Position)
            )
            .ThenInclude(gridRows => gridRows.Columns
                .OrderBy(column => column.Position)
            )
            .FirstOrDefault();

        // Create will initialize a new round with grid of 0 moves.
        public Round Create()
        {
            int gridSize = GetGridSize();
            int maxLightsOn = GetMaxLightsOn();
            int minLightsOn = GetMinLightsOn();

            // Initialize a round with an empty Grid.
            Round round = new Round
            {
                Size = gridSize,
                State = RoundState.Generated,
                Rows = Enumerable.Range(0, gridSize)
                    .Select((_, rowIndex) => new Row
                    {
                        Position = rowIndex,
                        Columns = Enumerable.Range(0, gridSize)
                            .Select((_, columnIndex) => new Column { Position = columnIndex })
                            .ToList()
                    })
                    .ToList(),
            };

            if (maxLightsOn > gridSize)
                maxLightsOn = gridSize;

            // Create a random number between the minLightsOn and the maxLightsOn above.
            int turnOn = _rnd.Next(minLightsOn, maxLightsOn);

            // Based on the random number we just generated, we're going to generate
            // a random row and column for every iteration. Inside the iteration we're
            // creating a condition to ensure that the generated row and column aren't
            // turned on already.
            for (int offset = 0; offset < turnOn; offset++)
            {
                int row;
                int column;
                bool on = false;

                do
                {
                    row = _rnd.Next(gridSize);
                    column = _rnd.Next(gridSize);

                    // If the light at the generated random row and columns is turned off
                    // We'll turn if on, and change the boolean above to true to stop the iteration.
                    if (!round.Rows.ElementAt(row).Columns.ElementAt(column).On)
                    {
                        on = true;
                        round.Rows.ElementAt(row).Columns.ElementAt(column).On = true;
                    }
                } while(!on);
            }

            _context.Add(round);
            _context.SaveChanges();

            return round;
        }

        public Round Move(Round round, Move move)
        {
            round = _manager.Move(round, move);

            _context.Rounds.Update(round);
            _context.SaveChanges();

            return round;
        }

        public void Forfeit(Guid gid)
        {
            var round = GetByID(gid);

            if (round == null)
                return;

            round = _manager.Forfeit(round);

            _context.Rounds.Update(round);
            _context.SaveChanges();
        }
    }
}
