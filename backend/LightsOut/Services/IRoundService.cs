﻿using LightsOut.Models;
using System;
using System.Collections.Generic;

namespace LightsOut.Services
{
    public interface IRoundService
    {
        public Round Create();

        public Round Move(Round round, Move move);

        public Round GetByID(Guid gid);

        public void Forfeit(Guid gid);
    }
}
