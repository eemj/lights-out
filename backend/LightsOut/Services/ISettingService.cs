﻿using LightsOut.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LightsOut.Services
{
    public interface ISettingService
    {
        public IEnumerable<Setting> GetAll();

        public Setting Update(string name, int value);

        public Setting GetByName(string name);
    }
}
