﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace LightsOut.Models
{
    public class Column
    {
        public Guid ID { get; set; }

        public bool On { get; set; }

        public int Position { get; set; }

        [JsonIgnore]
        public virtual Row Row { get; set; }
    }
}
