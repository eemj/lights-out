﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LightsOut.Models
{
    public class Move
    {
        public Guid ID { get; set; }

        // X defines the column position in the grid.
        public int X { get; set; }

        // Y defines the row position in the grid.
        public int Y { get; set; }
    }
}
