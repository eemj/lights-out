﻿using System;
using System.Collections.Generic;

namespace LightsOut.Models
{
    public class Round
    {
        public Guid ID { get; set; }

        public int Size { get; set; }

        public RoundState State { get; set; } = RoundState.Generated;

        public ICollection<Row> Rows { get; set; }

        public virtual ICollection<Move> Moves { get; set; }
    }
}
