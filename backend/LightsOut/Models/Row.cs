﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace LightsOut.Models
{
    public class Row
    {
        public Guid ID { get; set; }

        public int Position { get; set; }

        public virtual ICollection<Column> Columns { get; set; }

        [JsonIgnore]
        public virtual Round Round { get; set; }
    }
}
