﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LightsOut.Models
{
    public enum RoundState
    {
        Generated,
        InProgress,
        Canceled,
        Won,
    }
}
