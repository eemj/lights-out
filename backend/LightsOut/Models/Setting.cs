﻿using Microsoft.EntityFrameworkCore;
using System;

namespace LightsOut.Models
{
    [Index(nameof(Name), IsUnique = true)]
    public class Setting
    {
        public Guid ID { get; set; }

        public string Name { get; set; }

        public int Value { get; set; }
    }
}
