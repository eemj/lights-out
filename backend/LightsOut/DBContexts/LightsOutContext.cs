﻿using LightsOut.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;

namespace LightsOut.DBContexts
{
    public class LightsOutContext : DbContext
    {
        public LightsOutContext() { }

        public LightsOutContext(DbContextOptions options) : base(options)
        {
        }

        private IEnumerable<Setting> DefaultSettings()
        {
            return new List<Setting>
            {
                new Setting
                {
                    ID = Guid.NewGuid(),
                    Name = "GRID_SIZE",
                    Value = 5
                },
                new Setting
                {
                    ID = Guid.NewGuid(),
                    Name = "MAX_LIGHTS_ON",
                    Value = 5,
                },
                new Setting
                {
                    ID = Guid.NewGuid(),
                    Name = "MIN_LIGHTS_ON",
                    Value = 1,
                },
                new Setting
                {
                    ID = Guid.NewGuid(),
                    Name = "FUN_MODE",
                    Value = 0,
                },
            };
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Round>().HasMany(round => round.Rows).WithOne(gridRow => gridRow.Round).IsRequired();
            builder.Entity<Row>().HasMany(gridRow => gridRow.Columns).WithOne(gridColumn => gridColumn.Row).IsRequired();

            builder.Entity<Setting>().HasData(DefaultSettings());

            base.OnModelCreating(builder);
        }
        
        public DbSet<Move> Moves { get; set; }

        public DbSet<Round> Rounds { get; set; }

        public DbSet<Setting> Settings { get; set; }

        public DbSet<Row> GridRows { get; set; }

        public DbSet<Column> GridColumns { get; set; }
    }
}
