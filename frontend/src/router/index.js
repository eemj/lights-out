import Vue from "vue";
import VueRouter from "vue-router";

import Round from "@/views/Round.vue";
import Settings from "@/views/Settings.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: { path: "/round" },
    meta: {
      title: 'Lights Out | Redirecting..'
    },
  },
  {
    path: "/round/:id",
    name: "Specific Lights Out",
    component: Round,
    meta: {
      title: 'Lights Out | Round'
    },
  },
  {
    path: "/round",
    name: "Lights Out",
    component: Round,
    includeInSidebar: true,
    icon: "bulb",
    meta: {
      title: 'Lights Out | Round'
    },
  },
  {
    path: "/settings",
    name: "Settings",
    component: Settings,
    includeInSidebar: true,
    icon: "options",
    meta: {
      title: 'Lights Out | Settings'
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.afterEach((to) => {
  if (to.meta && to.meta.title) {
    document.title = to.meta.title
  }
})

export default router;
