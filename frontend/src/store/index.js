import Vue from "vue";
import Vuex, { createLogger } from "vuex";

import games from "@/store/modules/games";
import settings from "@/store/modules/settings";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    games,
    settings,
  },
  plugins: [createLogger()],
});
