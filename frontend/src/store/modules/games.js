import { CONTEXT } from "@/shared/path";

export const GameMutations = {
  GAME_PENDING: "GAME_PENDING",
  GAME_SUCCESS: "GAME_SUCCESS",
  GAME_FAILURE: "GAME_FAILURE",
  GAME_RESET: "GAME_RESET",
};

const defaultState = {
  loading: true,
  data: {},
  error: null,
};

const mutations = {
  [GameMutations.GAME_RESET](state) {
    state.data = {};
  },
  [GameMutations.GAME_PENDING](state) {
    state.loading = true;
    state.error = null;
  },
  [GameMutations.GAME_SUCCESS](state, data) {
    state.loading = false;
    state.data = data;
    state.error = null;
  },
  [GameMutations.GAME_FAILURE](state, error) {
    state.loading = false;
    state.data = {};
    state.error = error;
  },
};

const actions = {
  async createGame(context) {
    context.commit(GameMutations.GAME_RESET);
    context.commit(GameMutations.GAME_PENDING);

    try {
      const response = await window
        .fetch(`${CONTEXT}/api/Round/Create`)
        .then((res) => res.json());

      context.commit(GameMutations.GAME_SUCCESS, response);
    } catch (e) {
      context.commit(GameMutations.GAME_FAILURE, e.message);
    }
  },

  async getGame(context, id) {
    if (
      Object.keys(context.state.data).length > 0 &&
      context.state.data.id === id
    ) {
      return;
    }

    context.commit(GameMutations.GAME_RESET);
    context.commit(GameMutations.GAME_PENDING);

    try {
      const response = await window
        .fetch(`${CONTEXT}/api/Round/${id}`)
        .then((res) => res.json());

      context.commit(GameMutations.GAME_SUCCESS, response);
    } catch (e) {
      context.commit(GameMutations.GAME_FAILURE, e.message);
    }
  },

  async forfeitGame(context) {
    if (!context.state.loading) {
      const { id } = context.state.data;

      context.commit(GameMutations.GAME_PENDING);

      try {
        await window.fetch(`${CONTEXT}/api/Round/${id}`, {
          method: "DELETE",
        });

        context.commit(GameMutations.GAME_SUCCESS, {});
      } catch (e) {
        context.commit(GameMutations.GAME_FAILURE, e.message);
      }
    }
  },

  async makeMove(context, move) {
    if (
      !context.state.loading &&
      Object.keys(context.state.data).length > 0 &&
      context.state.data.id
    ) {
      const { id } = context.state.data;

      context.commit(GameMutations.GAME_PENDING);

      try {
        const response = await window
          .fetch(`${CONTEXT}/api/Round/${id}`, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(move),
          })
          .then((res) => res.json());

        context.commit(GameMutations.GAME_SUCCESS, response);
      } catch (e) {
        context.commit(GameMutations.GAME_PENDING, e.message);
      }
    }
  },
};

export default {
  state: defaultState,
  mutations,
  actions,
};
