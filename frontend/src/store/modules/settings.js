import { CONTEXT } from "@/shared/path";

export const SettingMutations = {
  SETTING_PENDING: "SETTING_PENDING",
  SETTING_SUCCESS: "SETTING_SUCCESS",
  SETTING_FAILURE: "SETTING_FAILURE",
  SETTING_RESET: "SETTING_RESET",
  SETTING_UPDATE: "SETTING_UPDATE",
};

const defaultState = {
  loading: false,
  data: {},
  error: null,
};

const mutations = {
  [SettingMutations.SETTING_PENDING](state) {
    state.loading = true;
    state.error = null;
  },
  [SettingMutations.SETTING_UPDATE](state, setting) {
    state.loading = false;
    state.error = null;

    // update the setting value by it's name.
    if (Object.keys(state.data).length > 0 && setting.name in state.data) {
      state.data[setting.name] = Number(setting.value);
    }
  },
  [SettingMutations.SETTING_SUCCESS](state, settings) {
    // convert the array to a map.
    const map = {};
    settings.forEach((setting) => {
      map[setting.name] = Number(setting.value);
    });

    state.loading = false;
    state.data = map;
    state.error = null;
  },
  [SettingMutations.SETTING_FAILURE](state, error) {
    state.loading = false;
    state.data = {};
    state.error = error;
  },
  [SettingMutations.SETTING_RESET](state) {
    state.data = {};
  },
};

const actions = {
  async getSettings(context) {
    if (context.state.loading) {
      return;
    }

    context.commit(SettingMutations.SETTING_RESET);
    context.commit(SettingMutations.SETTING_PENDING);

    try {
      const response = await window
        .fetch(`${CONTEXT}/api/Setting`)
        .then((res) => res.json());

      context.commit(SettingMutations.SETTING_SUCCESS, response);
    } catch (e) {
      context.commit(SettingMutations.SETTING_FAILURE, e.message);
    }
  },

  async updateSetting(context, setting) {
    if (context.state.loading) {
      return;
    }

    context.commit(SettingMutations.SETTING_PENDING);

    try {
      const response = await window
        .fetch(`${CONTEXT}/api/Setting`, {
          method: "PATCH",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(setting),
        })
        .then((res) => res.json());

      context.commit(SettingMutations.SETTING_UPDATE, response);
    } catch (e) {
      context.commit(SettingMutations.SETTING_FAILURE, e.message);
    }
  },
};

export default {
  state: defaultState,
  mutations,
  actions,
};
