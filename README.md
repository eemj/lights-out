# Lights Out

The following project has been created using Vue 2.x along with ASP.NET Core API. An API has been developed to create games and make moves on the board. Making moves on the backend was the number one priority to prevent cheaters, as the checks are done from the backend side and with every move, the board's lights are updated on the backend side and sent to the client side. When making a move, we're checking if there's still any lights on and if there isn't we'll let the user know that they won.

## Installation

1. Update the `DefaultConnection` connection string for your database in `backend/LightsOut/appsettings.json`.
2. Run the migrations to create tables for our models.
3. Host the API using a method of your choice.
4. API context path needs to be changed in `frontend/src/shared/path.js` to point the hosted backend.
5. Install the required modules for the frontend by navigating to frontend and run `npm install`.
6. Run the frontend using `npm run serve` or `yarn serve`.